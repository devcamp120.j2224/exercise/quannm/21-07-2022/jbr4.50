package com.devcamp.jbr450.jbr450;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;
    
    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public double deposit(double amount) {
        this.balance = balance + amount;
        return this.balance;
    }

    public double withdraw(double amount) {
        if (this.balance >= amount) {
            this.balance = balance - amount;
        }
        else {
            System.out.println("amount withdrawn exceeds the current balance!");
        }
        return this.balance;
    }

    @Override
    public String toString() {
        return getCustomerName() + "(" + this.id + ") balance=$" + this.balance;
    }
}

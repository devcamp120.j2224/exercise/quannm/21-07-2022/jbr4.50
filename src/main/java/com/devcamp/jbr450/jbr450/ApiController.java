package com.devcamp.jbr450.jbr450;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount() {
        ArrayList<Account> listAcc = new ArrayList<Account>();

        Customer customer1 = new Customer(1, "QuanNM", 10);
        Customer customer2 = new Customer(2, "BoiHB", 15);
        Customer customer3 = new Customer(3, "ThuNHM", 20);

        Account account1 = new Account(11, customer1);
        Account account2 = new Account(99, customer2, 200);
        Account account3 = new Account(16, customer3, 150);

        listAcc.add(account1);
        listAcc.add(account2);
        listAcc.add(account3);

        return listAcc;
    }

    // public static void main(String[] args) {
    //     Customer customer1 = new Customer(1, "QuanNM", 10);
    //     Customer customer2 = new Customer(2, "BoiHB", 15);
    //     Customer customer3 = new Customer(3, "ThuNHM", 20);
    //     System.out.println(customer1 + "," + customer2 + "," + customer3);
    //     System.out.println("---------------------");
    //     Account account1 = new Account(11, customer1);
    //     Account account2 = new Account(99, customer2, 200);
    //     Account account3 = new Account(16, customer3, 150);
    //     System.out.println(account1 + "," + account2 + "," + account3);
    // }
}
